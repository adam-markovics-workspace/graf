package hu.szoftlab4.rational;


import hu.szoftlab4.rational.Robot.RobotState;

import java.util.LinkedHashSet;
import java.util.Set;


/**
 * A j�t�k f� logik�j�nak kezel�s��rt felel�s oszt�ly
 * @author Daniel
 *
 */
public class Game {
	
	
	/**
	 * J�t�kosok robotjai
	 */
	private static Set<Robot> robots;
	
	/**
	 * P�ly�t megval�s�t� objektum
	 */
	private Track track;
	
	/**
	 * H�tral�v� k�r�k sz�ma
	 */
	private static int currentTurn;
	
	/**
	 * Verseny maxim�lis fordul�inak sz�ma
	 */
	private final int TURNS_MAX = 30; 
	
	/**
	 * J�t�k futtat�s�nak �llapota
	 */
	private GameState state;
	
	//private Thread gamethread;
	/**
	 * 
	 * @author Daniel
	 * Lehets�ges m�k�d�si �llapotok a j�t�k futtat�sa sor�n
	 */
	public enum GameState{
	    INIT, RUNNING, OVER
	}
	
	
	/**
	 * Aktu�lisan visszamarad� k�r�k sz�m�t visszaad� met�dus (getter)
	 * @return visszamaradt k�r�k sz�ma
	 */
	public static int getCurrentTurn(){
		return currentTurn;
	}
	
	/**
	 * Robotok halmaz�t visszaad� met�dus (Controller-nek)
	 * @return Robotok halmaza
	 */
	public static Set<Robot> getRobots(){
		return robots;
	}
	
	/**
	 * Param�ter n�lk�li konstruktor
	 */
	public Game(){
		
		currentTurn = TURNS_MAX;
		robots = new LinkedHashSet<Robot>();
		track = Track.getInstance();
		initGame();
		/*
		gamethread=new GameThread();
		gamethread.run();
		*/
	}
	
	/**
	 * J�t�k fut�s�nak �llapot�t lek�rdez� met�dus (getter)
	 * @return J�t�k fut�si �llapota
	 */
	public GameState getState(){
		return state;
	}
	
	/**
	 * J�t�k v�g�llapot�t felder�t� met�dus (els�sorban �tk�z�s miatti)
	 */
	public void isGameOver(){
		for(Robot r: robots){
			if(!r.isAlive() || currentTurn==0){
				state=GameState.OVER;
			}
		}
	}
	
	/**
	 * Kil�p�s az alkalmaz�sb�l
	 */
	public void exitGame(){
			track.destroy();
			track = null;
			robots.clear();
	}
	
	/**
	 * Verseny kezdete el�tt a P�lya fel�p�t�se (akad�lyokkal) �s a Robotok inicializ�l�sa
	 */
	public void initGame(){
			state = GameState.INIT;			// Init �llapotba l�p�s
			track.initialize();	// Track inicializ�l�sa �s l�trehozott robotok be�ll�t�sa a Game-ben
			robots = track.getRobots();
			
	}
	
	/**
	 * Egy fordul� lefuttat�sa
	 */
	private void nextTurn(){
			track.update(); 				// Track �rtes�t�se, hogy az �reg�t�seket tegye meg az �jabb k�r kezdete miatt
			
			if (robots!=null){
				for(Robot robot: robots){		// �sszes Robot l�ptet�se
					if(!Track.getInstance().getReachableGridPoints(robot).isEmpty()){
						robot.setState(RobotState.IDLING);
					} else {
						robot.setState(RobotState.LASTJUMP);
					}
					robot.move();
				}
			}
			
			track.notifyObservers();		// Track �rtes�t�se a k�rv�gi friss�t�sek�rt --> K�v k�r elej�n �reg�t�nk

	}	
	
	/**
	 * Fordul�k lefuttat�sa egym�s ut�n
	 */
	private void runTurns(){
			for (; currentTurn > 0; currentTurn--) {	// Ha lett fordul�sz�m be�ll�tva, akkor j�t�k
				if(state==GameState.RUNNING){
					
					// Minden k�r elej�n tesztelj�k, hogy nem-e jutottunk-e gy�ztes szitu�ci�ba az el�z� k�rben
					isGameOver();
					if(getState()==GameState.OVER){
						Controller.displayResults();	
					} else {
						nextTurn();	// K�r�k l�ptet�se
					}		
				}
				else if(state==GameState.INIT){
					state=GameState.RUNNING;
				}	
				else if(state==GameState.OVER){
					return;								//t�bb k�rt m�r nem futtatunk
				}	
			}
			// Ha ler�jt minden fordul�, akkor mindenk�ppen eredm�ny:
			Controller.displayResults();
	}
	
	/**
	 * Verseny fut�s�t vez�rl� logika (Fordul�k �s l�p�sek kezel�se)
	 */
	public void runGame(){
		state=GameState.RUNNING;
			runTurns(); //minden fordul� lefuttat�sa
	}
	
	/**
	 * Verseny elind�t�sa TODO jelenleg nem haszn�lom
	 */
	public void startGame(){
			initGame();		// Inicializ�l�s
			runGame();		// J�t�klogika futtat�sa

	}
}
