package hu.szoftlab4.rational;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.geom.Rectangle2D;
import java.util.List;

/**
 * Robot megjelen�t�s�rt felel�s oszt�ly
 * @author Daniel
 *
 */
public class RobotView implements Drawable{

	/**
	 * Referencia a kirajzoland� Robotra
	 */
	private Robot robot;
	
	/**
	 * Kirajzoland� Robot "mint�ja"
	 */
	private Rectangle2D shape;

	/**
	 * Param�teres konstruktor
	 * @param r Hozz� tartoz� Robot referenci�ja
	 */
	public RobotView(Robot r){
		robot=r;
		shape=new Rectangle(80+robot.getCurrentGridPoint().getCoords().getX()*16,
				120+robot.getCurrentGridPoint().getCoords().getY()*12,10,10);		
	}
	
	/**
	 * Kirajzol�st v�gz� met�dus
	 */
	@Override
	public void draw(Graphics graphics) {
		Graphics2D graphics2d = (Graphics2D)graphics;
		shape.setRect(80+robot.getCurrentGridPoint().getCoords().getX()*16,
				120+robot.getCurrentGridPoint().getCoords().getY()*12,10,10);
		if(robot.getID()==1){
			graphics2d.setColor(Color.RED);
		}
		else {
			graphics2d.setColor(Color.BLUE);
		}
		graphics2d.fill(shape);
		
		drawReachable(graphics2d);
	}
	
	/**
	 * El�rhet� (v�laszthat�) r�cspontok kirajzol�sa (ugr�s el�tt az aktu�lis robot �ltal v�laszthat� r�cspontok l�tszanak)
	 * @param graphics2d Kirajzol�shoz sz�ks�ges referencia
	 */
	private void drawReachable(Graphics2D graphics2d){
		if(robot.getState()==Robot.RobotState.IDLING){
			List<GridPoint> reachableGridPointList = Track.getInstance().getReachableGridPoints(robot);
			for(GridPoint reachableGridPoint: reachableGridPointList){
				if(robot.getID()==1){
					graphics2d.setColor(new Color(255,153,153));
					graphics2d.fillOval(80+reachableGridPoint.getCoords().getX()*16,120+reachableGridPoint.getCoords().getY()*12,8,8);
					graphics2d.setColor(Color.BLACK);
				}
				else{
					graphics2d.setColor(new Color(153,153,255));
					graphics2d.fillOval(80+reachableGridPoint.getCoords().getX()*16,120+reachableGridPoint.getCoords().getY()*12,8,8);
					graphics2d.setColor(Color.BLACK);
				}
			}			
		}
	}

	/**
	 * Robot kirajzol�si precedenci�j�t visszaad� met�dus
	 */
	@Override
	public int getZ() {
		return 3;
	}


}
