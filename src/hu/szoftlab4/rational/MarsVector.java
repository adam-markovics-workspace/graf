package hu.szoftlab4.rational;

/**
 * Seg�doszt�ly az abszol�t poz�ci�k meghat�roz�s�ra, illetve a robotok sebess�geinek m�r�s�re
 * @author Daniel
 *
 */
public class MarsVector {
	/**
	 * Sz�less�gi koordin�ta
	 */
	private int x;
	
	/**
	 * Magass�gi koordin�ta
	 */
	private int y;
	
	/**
	 * Default konstruktor
	 */
	public MarsVector(){
		x=0;
		y=0;
	}
	
	/**
	 * Param�terezett konstruktor
	 * @param x Sz�less�gi koordin�ta
	 * @param y Magass�gi koordin�ta
	 */
	public MarsVector(int x, int y){
		this.x = x;
		this.y = y;
	}
	
	/**
	 * X koordin�ta �rt�k�nek lek�rdez�se
	 * @return  �rt�k int-k�nt
	 */
	public int getX() {
		return x;
	}
	
	/**
	 * Y koordin�ta �rt�k�nek lek�rdez�se
	 * @return  �rt�k int-k�nt
	 */
	public int getY() {
		return y;
	}
	
	/**
	 * "Orig�t�l" vett t�vols�g lek�rdez�se
	 * @return  T�vols�g double-k�nt
	 */
	public double length(){
		return Math.sqrt(x*x+y*y);
	}
	
	/**
	 * MarsVectorokat �sszead� oper�tor
	 * @param other M�velethez sz�ks�ges m�sik MarsVector
	 * @return M�velet eredm�nye
	 */
	public MarsVector add(MarsVector other){
		return new MarsVector(this.x+other.x, this.y+other.y);
	}
	
	/**
	 * MarsVectorokat �sszead� oper�tor
	 * @param other M�velethez sz�ks�ges m�sik MarsVector
	 */
	public void addToThis(MarsVector other){
		this.x += other.x;
		this.y += other.y;
	}
	
	/**
	 * MarsVectorokat egym�sb�l kivon� oper�tor
	 * @param other M�velethez sz�ks�ges m�sik MarsVector
	 * @return M�velet eredm�nye
	 */
	public MarsVector subtract(final MarsVector other){
		return new MarsVector(this.x-other.x, this.y-other.y);
	}
	
	/**
	 * MarsVectorokat egym�sb�l kivon� oper�tor
	 * @param other M�velethez sz�ks�ges m�sik MarsVector
	 */
	public void subtractFromThis(MarsVector other){
		this.x -= other.x;
		this.y -= other.y;
	}
	
	/**
	 * Koordin�tap�rt kiirat� met�dus
	 */
	public String toString(){
		return new String(this.x + "," + this.y);
	}
	
	/**
	 * GridPoint-t� konvert�l� met�dus
	 * @return Konvert�lt GridPoint
	 */
	public GridPoint convertToGridPoint(){
		return Track.getInstance().marsVectorToGridPoint(this);
	}
	
	/**
	 * K�t MarsVector-t �sszahonl�t�s�t v�gz� met�dus
	 * @param other �sszehasonl�tand� MarsVector az aktu�lissal
	 * @return �sszehasonl�t�s eredm�nye: true or false
	 */
	public boolean equals(MarsVector other){
		if (this.x== other.x && this.y == other.y){
			return true;
		}
		else{
			return false;
		}		
	}
}
