package hu.szoftlab4.rational;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.swing.JButton;
import javax.swing.JPanel;

/**
 * Kirajzol�s�rt felel�s oszt�ly ami t�rolja a kirajzoland� (Drawable) objektumokat
 * @author Daniel
 *
 */
public class View extends JPanel{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Rajzol�s �llapot�tm eghat�roz� enum (RUNNING:J�t�kt�r vs OVER:V�geredm�ny)
	 * @author Daniel
	 *
	 */
	public enum ViewState{
	    RUNNING, OVER
	}
	
	/**
	 * J�t�k �llapota (mely meghat�rozza, mit rajzolunk ki a k�perny�re)
	 */
	ViewState state;
	
	/**
	 * Olajhaszn�latot v�gz� gomb
	 */
	JButton oilButton;
	
	/**
	 * Ragacshaszn�latot v�gz� gomb
	 */
	JButton gooButton;
	
	/**
	 * Szingleton minta alapj�n egyetlen view t�pus� objektum
	 */
	private static View instance = new View();
	
	/**
	 * Kirajzoland� objektumok list�ja
	 */
	List<Drawable> drawables;
	
	/**
	 * H�tramarad� fordul�k t�rol�sa lok�lisan
	 */
	private int remainingTurns;	 
	
	/**
	 * P�ly�n el�rhet� Robotok t�rol�sa lok�lisan a k�nnyebb inform�ci�lek�rdez�s v�gett
	 */
	private List<Robot> robotList;
	 
	/**
	 * Kirajzolhat� objektumokat lek�rdez� met�dus (getter)
	 * @return kirajzolhat� objektumok list�ja
	 */
	public List<Drawable> getDrawables(){
		return drawables;
	}
	 
	 /**
	  * View param�ter n�lk�li konstruktora
	  */
	 private View(){
	  setFocusable(true); 
	  requestFocus();
	  drawables=new ArrayList<Drawable>();
	  oilButton=new JButton("Olaj");
	  gooButton=new JButton("Ragacs");
	  oilButton.setBounds(360, 60, 80, 20);
	  gooButton.setBounds(360, 90, 80, 20);
	  this.setLayout(null);
	  this.add(oilButton);
	  this.add(gooButton);
	  state=ViewState.RUNNING;
	 }
	
	 /**
	  * Lok�lisan t�rolt inform�ci�k friss�t�se k�r�nk�nt
	  * @param robotlist Robotokok list�ja
	  * @param t H�trel�v� k�r�k sz�ma
	  */
	void update(Set<Robot> robotlist, int t){
		this.robotList = new ArrayList<Robot>(robotlist);
		remainingTurns=t;
		if(state==ViewState.OVER){
			this.remove(oilButton);
			this.remove(gooButton);
		}
	}
	
	
	/**
	 * Szingleton pattern alapj�n a View referenci�t elk�r� met�dus
	 * @return View referencia
	 */
	public static View getInstance() {
		if (instance == null) {
			instance = new View();
		}
		return instance;
	}
	
	/**
	 * J�t�kmenetet �llapot�t be�ll�t� met�dus (setter)
	 * @param v �tadott j�t�k�llapot: RUNNING, OVER
	 */
	public void setState(ViewState v){
		state=v;
	}
	

	/**
	 * Kirajzol� met�dus
	 */
	public void paintComponent(Graphics g) {
			Graphics2D g2d=(Graphics2D)g;
			super.paintComponent(g2d);
			if(state==ViewState.RUNNING){
				g2d.setColor(Color.BLACK);
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				for (int layer=0; layer<=3; layer++){
					for (Drawable drawable : drawables){
						if (drawable.getZ()==layer){
							drawable.draw(g);
						}
					}
				}			
				
				g2d.setColor(Color.BLACK);
				g2d.setFont(new Font(Font.SANS_SERIF, Font.ITALIC, 20));
				g2d.drawString("Red Robot: "+robotList.get(0).getScore()+" pont",0,20);
				g2d.drawString("Olajk�szlet: "+robotList.get(0).getOilCount(),0,50);
				g2d.drawString("Ragacsk�szlet: "+robotList.get(0).getGooCount(),0,80);
				
				//hogy kapjuk meg ide
				g2d.drawString("H�tral�v� fordul�k: "+remainingTurns,300,20);
				
				g2d.drawString("Blue Robot: "+robotList.get(1).getScore()+" pont",540,20);
				g2d.drawString("Olajk�szlet: "+robotList.get(1).getOilCount(),540,50);
				g2d.drawString("Ragacsk�szlet: "+robotList.get(1).getGooCount(),540,80);
				
			}else{

				g2d.setColor(Color.RED);
				g2d.setFont(new Font(Font.SANS_SERIF, Font.ITALIC, 20));
				g2d.drawString("Red Robot points: "+robotList.get(0).getScore(),300, 240);
				g2d.setColor(Color.BLUE);
				g2d.drawString("Blue Robot points: "+robotList.get(1).getScore(),300, 280);
				
				
				boolean anydead=false;
				for(Robot r : robotList){
					if(!r.isAlive()) anydead=true;
				}
				if(anydead){
					for(Robot r : robotList){
						if(r.isAlive()){
							if(r.getID()==1){
								printWinner(WinnerRobotColor.RED, g2d);
							}
							else {
								printWinner(WinnerRobotColor.BLUE, g2d);
							}
						}
					}
				}
				else{
							if(robotList.get(0).getScore()>robotList.get(1).getScore()){
								printWinner(WinnerRobotColor.RED, g2d);
							}
							else if(robotList.get(0).getScore()==robotList.get(1).getScore()){
								printWinner(WinnerRobotColor.NONE, g2d);
							}
							else {
								printWinner(WinnerRobotColor.BLUE, g2d);
							}
				}
			}
		}
	
	/**
	 * Eredm�nyjelz�s megjelen�t�s�t seg�t�, Robot beazonos�t�s�ra szolg�l� bels� enum
	 * @author Daniel
	 *
	 */
	private enum WinnerRobotColor { BLUE, RED, NONE }


	/**
	 * Gy�ztes ki�rat�s�t v�gz� met�dus
	 * @param r Megkapott Robot RobotState alap� azonos�t�ja
	 * @param g2d Rajzol� referencia
	 */
	private void printWinner(WinnerRobotColor r, Graphics2D g2d){
		g2d.setFont(new Font(Font.SERIF,Font.BOLD, 50));
		if (r==WinnerRobotColor.BLUE){
			g2d.setColor(Color.BLUE);
			g2d.drawString("Blue robot won!",250, 330);
		} else if (r==WinnerRobotColor.RED){
			g2d.setColor(Color.RED);
			g2d.drawString("Red robot won!",250,330);
		} else if(r==WinnerRobotColor.NONE){
			g2d.setColor(Color.BLACK);
			g2d.drawString("D�ntetlen!!!!!!",250, 330);
		}
	}

	/**
	 * �j kirajzolhat� objektum beregisztr�l�sa
	 * @param d Kirajzoland� objektum
	 */
	public void addDrawable(Drawable d){
		drawables.add(d);
	}
	
	/**
	 * Megsz�nt kirajzoland� objektum kiregisztr�l�sa
	 * @param d Kiregisztr�land� objektum
	 */
	public void removeDrawable(Drawable d){
		drawables.remove(d);
	}

}