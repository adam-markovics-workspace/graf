package hu.szoftlab4.rational;

/**
 * Mozg�st v�gz� elementek (MiniRobot �s Robot) k�z�s absztrack �soszt�lya
 * @author Daniel
 *
 */
public abstract class Machine implements Element, ElementVisitor {

	/**
	 * Aktu�lis r�cspont
	 */
	protected GridPoint currentGridPoint;
	
	/**
	 * currentGridPoint getter met�dusa
	 * @return Ezen a r�csponton tart�zkodik a robot
	 */
	public GridPoint getCurrentGridPoint(){
		return this.currentGridPoint;
	}
	
	/**
	 * currentGridPoint setter met�dusa
	 * @param gp Megadott l�tez� r�cspont amin a robot tart�zkodik
	 */
	protected void setCurrentGridPoint(GridPoint gp){
		this.currentGridPoint.removeElement(this);	//elvessz�k a r�gir�l
		this.currentGridPoint = gp; 				//�j hely be�ll�t�sa
		this.currentGridPoint.addElement(this);		//hozz�adjuk az �jhoz
	}
	/**
	 * P�ly�n mozg�st v�gz� elementek (MiniRobot �s Robot) k�z�s mozgat�s�t v�gz� abstract met�dusa
	 */
	public abstract void move(); 
	
}
