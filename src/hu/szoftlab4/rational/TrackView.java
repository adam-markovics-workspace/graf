package hu.szoftlab4.rational;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.ArrayList;

/**
 * P�lya megjelen�t�s�rt felel�s oszt�ly
 * @author Daniel
 *
 */
public class TrackView implements Drawable{

	/**
	 * P�lya kirajzol�s�t v�gz� met�dus
	 */
	@Override
	public void draw(Graphics graphics) {
		Graphics2D graphics2d=(Graphics2D)graphics;
		graphics2d.setColor(Color.BLACK);
		ArrayList< ArrayList<GridPoint> > grid=Track.getInstance().getGrid();
		for(ArrayList<GridPoint> gridLine: grid){
			for(GridPoint gridPoint: gridLine){
				drawGridPoint(graphics2d, gridPoint);
			}
		}	
	}

	/**
	 * P�lya r�cspontjait v�gz� met�dus
	 * @param graphics2d Kirajzol�shoz sz�ks�ges referencia
	 * @param gridPoint Kirajzoland� r�cspont
	 */
	private void drawGridPoint(Graphics2D graphics2d, GridPoint gridPoint){
		if(gridPoint.isValid()==true){
			graphics2d.drawOval(80+gridPoint.getCoords().getX()*16,120+gridPoint.getCoords().getY()*12, 8, 8);
		}
		else {
			graphics2d.fillOval(80+gridPoint.getCoords().getX()*16,120+gridPoint.getCoords().getY()*12, 10, 10);
		}			
	}
	
	/**
	 * P�lya kirajzol�si precedenci�j�t visszaad� met�dus
	 */
	@Override
	public int getZ() {
		return 0;
	}
	
}
