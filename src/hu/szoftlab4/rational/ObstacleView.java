package hu.szoftlab4.rational;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.geom.Rectangle2D;

/**
 * Akad�ly kirajzol�s��rt felel�s abstract �soszt�ly
 * @author Daniel
 *
 */
public abstract class ObstacleView implements Drawable{

	/**
	 * Referencia a kirajzoland� objektumra
	 */
	private Obstacle obstacle;
	
	/**
	 * Akad�ly kirajzoland� "minta" (Egys�ges minden akad�lyra, csak a sz�n t�r el)
	 */
	private Rectangle2D shape;
	
	/**
	 * Akad�lyok megjelen�t�se sz�ne (akad�lyonk�nt elt�r�)
	 */
	protected Color color;
	
	/**
	 * Param�teres konstruktor
	 * @param obstacle Referencia az akad�lyra
	 * @param color Akad�ly megjelen�t�si sz�ne
	 */
	public ObstacleView(Obstacle obstacle, Color color){
		this.obstacle=obstacle;
		shape=new Rectangle(80+obstacle.getPosition().getX()*16,
				120+obstacle.getPosition().getY()*12,10,10);	
		
		this.color = color;
	}

	/**
	 * Kirajzol�st v�gz� met�dus
	 */
	@Override
	public void draw(Graphics g) {
		Graphics2D g2d=(Graphics2D)g;
		
		
		shape.setRect(80+obstacle.getPosition().getX()*16,
				120+obstacle.getPosition().getY()*12,10,10);
		
		g2d.setColor(color);
		g2d.fill(shape);
		
	}

	/**
	 * Kirajzol�si precedenci�t visszaat� met�dus
	 */
	@Override
	public int getZ() {
		return 1;
	}

}
