package hu.szoftlab4.rational;

import java.awt.Graphics;

/**
 * Kirajzolhat� objektumok k�z�s interf�sze
 * @author Daniel
 *
 */
interface Drawable {

	/**
	 * Kirajzol� met�dus
	 * @param g Rajzol�shoz sz�ks�ges referencia objektum
	 */
	public void draw(Graphics g);
	
	/**
	 * Kirazoland� objektum rajzol�si precedenci�j�t lek�rdez� met�dus
	 * @return Kirajzol�si precedencia (kirajzol�si sorrendis�ghez)
	 */
	public int getZ();
	
}
