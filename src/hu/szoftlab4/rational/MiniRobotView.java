package hu.szoftlab4.rational;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.geom.Rectangle2D;

/**
 * MiniRobot megjelen�t�s��rt felel�s oszt�ly
 * @author Daniel
 *
 */
public class MiniRobotView implements Drawable{

	/**
	 * Referencia a megjelen�tend� minirobotra
	 */
	MiniRobot minirobot;
	
	/**
	 * MiniRobot megjelen�t�si "mint�ja"
	 */
	Rectangle2D shape;

	/**
	 * Param�teres konstruktor
	 * @param mini Referencia a minirobotra
	 */
	public MiniRobotView(MiniRobot mini){
		minirobot=mini;
		shape=new Rectangle(80+minirobot.getCurrentGridPoint().getCoords().getX()*16,
				120+minirobot.getCurrentGridPoint().getCoords().getY()*12,10,10);		
	}

	/**
	 * Kirajzol�s�rt felel�s met�dus
	 */
	@Override
	public void draw(Graphics graphics) {
		Graphics2D graphics2d=(Graphics2D)graphics;
		
		shape.setRect(80+minirobot.getCurrentGridPoint().getCoords().getX()*16,
				120+minirobot.getCurrentGridPoint().getCoords().getY()*12,10,10);

		
		graphics2d.setColor(Color.MAGENTA);
		graphics2d.fill(shape);		
	}

	/**
	 * Kirjazol�si precedenci�t visszaad� met�dus
	 */
	@Override
	public int getZ() {
		return 2;
	}

}
