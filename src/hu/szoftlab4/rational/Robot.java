package hu.szoftlab4.rational;


import java.util.LinkedHashSet;
import java.util.Set;


/**
 * J�t�kos �ltal ir�ny�that�, p�ly�n ugr�l� Robotot megval�s�t� oszt�ly
 * @author Daniel
 *
 */
public class Robot extends Machine{	
	
	/**
	 * Robot ragacsk�szlete
	 */
	private int gooCount = 3;
	
	/**
	 * Robot olajk�szlete
	 */
	private int oilCount = 3;
	
	/**
	 * Robot azonos�t�ja
	 */
	private int id;
	
	/**
	 * Annak jelz�se, hogy olajfolt hat�sa alatt van-e a robot
	 */
	private boolean oiled;
	
	/**
	 * Robot pontsz�ma
	 */
	private double score;
	
	/**
	 * Robot relat�v sebess�ge MarsVector-ban (El�z� ugr�s alapj�n)
	 */
	private MarsVector speed;

	/**
	 * Robot konstruktora, ami el is helyezi a kapott r�csponton
	 * @param gp Poz�ci� r�cspontja
	 */
	
	/**
	 * Roobt aktu�lis �llapota (DEAD or ALIVE)
	 */
	private RobotState state;
	
	/**
	 * Robot �llapotait le�r� Enum
	 * @author Daniel
	 *
	 */
	public enum RobotState{
	    ALIVE, IDLING, MOVING, DEAD, LASTJUMP
	}
	
	/**
	 * Robot azonos�t�j�t lek�r� met�dus
	 * @return robot azonos�t�ja
	 */
	public int getID(){
		return id;
	}
	
	/**
	 * Olajk�szletet lek�rdez� met�dus
	 * @return olajk�szlet mennyis�ge
	 */
	public int getOilCount(){
		return oilCount;
	}
	
	/**
	 * Ragacsk�szletet lek�rdez� met�dus
	 * @return ragacsk�szlet
	 */
	public int getGooCount(){
		return gooCount;
	}
	
	/**
	 *  Robot konstruktora (l�tez� r�csponttal �s kiindul�si r�csponttal param�terezve)
	 * @param id Robot azonos�t�ja
	 * @param gp Erre a r�cspontra reigsztr�ljuk be
	 */
	public Robot(int id, GridPoint gp) {
		state=RobotState.ALIVE;
		currentGridPoint=gp;
		speed = new MarsVector(0,0);
		this.id=id;
		this.currentGridPoint.addElement(this);
		Track.getInstance().addRobot(this);
		View.getInstance().addDrawable(new RobotView(this));
	}
	
	/**
	 * Robot �llapot�t lek�rdez� met�dus
	 * @return Robot �llapota
	 */
	public RobotState getState(){
		return state;
	}

	/**
	 * Robot �llapot�t be�ll�t� met�dis (ALIVE or DEAD)
	 * @param state Robot �llapota
	 */
	public void setState(RobotState state){
		this.state=state;
	}
	
	/**
	 * �letben van-e m�g a Robot (Nem ugrott ki a p�ly�r�l �s nem is ugrott r� m�sik robot)
	 * @return �letben van-e vagy sem
	 */
	public boolean isAlive(){
		if (!(state == RobotState.DEAD)) {
			return true;
		}
		return false;
	}
	

	/**
	 * Ugr�s a megadott r�cspontra
	 * @param gp2 Ugr�s c�l r�cspontja
	 */
	private void jump(GridPoint gp2){
			
		currentGridPoint.removeElement(this);			
		setCurrentGridPoint(gp2);
		this.setOiled(false); //Itt kell olajmentes�teni - az el�z� k�r olaja m�r nem sz�m�t, ha pedig most abba l�pett, az m�g csak a k�vetkez� sorokban j�n
		Set<Element> elementsCopy = new LinkedHashSet<Element>(gp2.getElements());
		for (Element element : elementsCopy){
			element.accept(this);
		}
		gp2.addElement(this);
	}
	
	/**
	 * Robot mozgat�s�nak vez�rl� met�dusa.
	 * A Game h�vja meg fordul�nk�nt 1-szer minden robotra
	 */
	@Override
	public void move(){
		
			//Ez az�rt, mert ha �tk�znek, a m�sik robot m�g esetleg l�phet a fordul�ban �s akkor ne tudjon m�r mozogni
			if(state!=RobotState.DEAD){
				while(state==RobotState.IDLING){			
					
					try {
						Thread.sleep(100);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				
				if(state==RobotState.MOVING){
					setState(RobotState.ALIVE);
					// Ugr�s megval�s�t�sa
					score+=speed.length();
					jump(this.currentGridPoint.getCoords().add(speed).convertToGridPoint());				// Ugr�s gp2-re
				} else if(state==RobotState.LASTJUMP){
					while(this.currentGridPoint.getCoords().add(speed).convertToGridPoint()==null){
						int x = speed.getX()-1;
						int y = speed.getY()-1;
						speed = new MarsVector(x > 0 ? x : 0, y > 0 ? y : 0);
					}
					jump(this.currentGridPoint.getCoords().add(speed).convertToGridPoint());
					setState(RobotState.DEAD);
				}
		}
	}
	
	/**
	 * Ragacs lerak�sa az aktu�lis r�cspontra az ugr�st megel�z�en
	 */
	public void useGoo(){
			//Singletonos megold�s Track-en kereszt�l
		if (gooCount>0 && !hasObstacle()){
			new Goo(currentGridPoint.getCoords(), 20);
			gooCount--;
		}
	}
	
	/**
	 * Olaj lerak�sa az aktu�lis r�cspontra az ugr�st megel�z�en
	 */
	public void useOil(){
			//Singletonos megold�s Track-en kereszt�l
		if (oilCount>0 && !hasObstacle()){
			new Oil(currentGridPoint.getCoords(), 20);
			oilCount--;
		}
	}
	
	/**
	 * Akad�ly haszn�lata el�tt az aktu�lis r�cspont tartalmaz-e m�r akad�lyt
	 * @return Tartalmaz-e m�r akad�lyt a currentGridPoint: true or false
	 */
	private boolean hasObstacle(){
		boolean occupied = false;
		for (Obstacle obstacle : Track.getInstance().getObstacles()){
			if (obstacle.getPosition().equals(currentGridPoint.getCoords())){
				occupied = true;
				break;
			}				
		}		
		return occupied;
	}
	
	/**
	 * Olajozotts�g lek�r�se
	 * @return olajozott-e a mez�: true vagy false
	 */
	public boolean isOiled() {
		return oiled;
	}

	/**
	 * Olajozotts�g elszenved�se a c�l r�cspontra �rkez�s sor�n
	 * @param oiled Olajozotts�got jelz� v�ltoz�
	 */
	private void setOiled(boolean oiled) {
		this.oiled = oiled;
	}
	
	/**
	 * Robot pontsz�m�nak lek�rdez�se
	 * @return Eddig el�rt pontsz�m
	 */
	public double getScore() {		
		return Math.round(score*100)/100.0;
	}


	/**
	 * Robot sebess�g�nek lek�rdez�se MarsVector-ban
	 * @return Aktu�lis sebess�g MarsVector-ban
	 */
	public MarsVector getSpeed() {
			return speed;
	}
		
	
	/**
	 * Robot Sebess�g�nek be�ll�t�sa MarsVector-b�l
	 * @param speed Be�ll�tand� sebess�g MarsVectorban
	 */
	public void setSpeed(MarsVector speed) {
		
			this.speed = speed;
	}
	
	/**
	 * Ragacsra �rkez�s elszenved�s�t kezel� met�dus
	 */
	@Override
	public void visit(Goo goo) {
		
			goo.stepOn();
			setSpeed(new MarsVector(speed.getX()/2, speed.getY()/2));
	}

	/**
	 * Olajra �rkez�s elszenved�s�t kezel� met�dus
	 */
	@Override
	public void visit(Oil oil) {
		
			setOiled(true);
	}

	/**
	 * Minirobotra �rkez�s elszenved�s�t kezel� met�dus
	 */
	@Override
	public void visit(MiniRobot miniRobot) {
		
		//MiniRobot r�cspontj�nak elment�se megsemmis�t�s el�tt
		MarsVector miniRobotPosition = miniRobot.getCurrentGridPoint().getCoords();
		
		// R�cspontr�l minden nem Robot elem megsemmis�t�se
		Set<Element> gridPointElementsCopy = new LinkedHashSet<Element>(currentGridPoint.getElements());
		for(Element element : gridPointElementsCopy){
			if(element!=this){
				element.kill();
			}
		}
		
		// �j olaj lerak�sa a mentett koordin�t�val (Robot currentGridPoint-ja nem biztos, hogy be van �ll�tva ekkor)
		new Oil(miniRobotPosition, 20);
		
		//A Minirobot olajos "v�re" sz�tfr�ccsen mindenfel�, a Robotot is beter�tve
		setOiled(true); 
	}
	
	/**
	 * M�sik Robotra �rkez�s elszenved�s�t kezel� met�dus
	 */
	@Override
	public void visit(Robot robot) {
		
			if(robot!=this){
				this.setState(RobotState.DEAD); //Az �tk�z�st okoz� robot kipurcan
			}
	}
	
	/**
	 * Jelz�se a r��rkez� objektum (robot vagy minirobot) fel�, hogy roboton landolt az ugr�s sor�n
	 */
	@Override
	public void accept(ElementVisitor elementVisitor) {
		
			elementVisitor.visit(this); //Az accept-nek nincs m�s dolga, csak hogy megh�vja a visit-et
	}

	/**
	 * Robot megsemmis�l�s��rt �s a Track-r�l val� kiregisztr�l�s�rt felel�s met�dus
	 */
	public void kill(){
		//TODO
		//Soha se hivjuk meg
	}
}
