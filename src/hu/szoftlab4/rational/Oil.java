package hu.szoftlab4.rational;

/**
 * Olajfoltot megval�s�t� oszt�ly
 * @author Daniel
 *
 */
public class Oil extends Obstacle{
	
	/**
	 * Olaj felsz�rad�si sebess�ge
	 */
	private static final int DRYING_RATE = 1;
	/**
	 * Param�ter n�lk�li konstruktor a skeleton futtat�s�hoz
	 */
	public Oil(){
		this(new MarsVector(), 20);
	}
	
	/**
	 * Olaj konstruktora
	 * @param pos Inicializ�lt koordin�ta
	 * @param i Inicializ�lt �letpontok
	 */
	public Oil(MarsVector pos, int i) {
		super(pos, i);
		addView(new OilView(this));
	}
//ez
	/**
	 * Visitor patternt megval�s�t� met�dus
	 * Jelzi a r�ugr� robot sz�m�ra, hogy olyan r�cspontra �rkezett amelyen olajfolt tal�lhat�
	 */
	@Override
	public void accept(ElementVisitor elementVisitor) {

			//Olajra l�p�s szeki
			elementVisitor.visit(this);
	}

	/**
	 * Olaj k�r�nk�nti �rtes�t�s��rt felel�s met�dus
	 */
	@Override
	public void notifyObstacle() {
		decreaseLifePoints(DRYING_RATE);
	}
		
}
