package hu.szoftlab4.rational;


import java.util.ArrayList;
import java.util.List;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * J�t�kteret megval�s�t� P�lya oszt�ly
 * @author Daniel
 *
 */
public class Track {
	
	/**
	 * P�ly�t alkot� r�cspontok list�ja
	 */
	private ArrayList< ArrayList<GridPoint> > grid;
	
	/**
	 * P�lya �sszes r�cspontj�t visszaad� met�dus (getter)
	 * @return �sszes r�cspont
	 */
	public ArrayList< ArrayList<GridPoint>> getGrid(){
		return grid;
	}
	
	/**
	 * Param�terben kapott robot �ltal el�rhet� r�cspontokat lek�rdez� met�dus az ugr�shoz
	 * @param r Robot, aminek keress�k az ugr�s lehets�ges r�cspontjait
	 * @return Lehets�ges (v�laszthat�) r�cspontok list�ja
	 */
	public List<GridPoint> getReachableGridPoints(Robot r){
		
		MarsVector robotPlace = r.getCurrentGridPoint().getCoords();
		MarsVector robotSpeed = r.getSpeed();
		MarsVector robotDest = robotPlace.add(robotSpeed);
		int destX = robotDest.getX();
		int destY = robotDest.getY();
				
		List<GridPoint> resultList = new ArrayList<GridPoint>();
		if (r.isOiled() && (robotPlace.getX() != robotDest.getX() || robotPlace.getY() != robotDest.getY())) {
			if (destX<width && destX>=0 && destY<height && destY>=0){
				MarsVector neighborMarsVector = new MarsVector(destX, destY);
				if(neighborMarsVector.convertToGridPoint().isValid()){
					resultList.add(neighborMarsVector.convertToGridPoint());
				}
			}
		}
		else {
			for (int x=-1; x<=1; x++){
				for (int y=-1; y<=1; y++){
					int neighborX = destX+x;
					int neighborY = destY+y;
					if (neighborX<width && neighborX>=0 && neighborY<height && neighborY>=0){
						MarsVector neighborMarsVector = new MarsVector(neighborX, neighborY);
						if(neighborMarsVector.convertToGridPoint().isValid()){
							resultList.add(neighborMarsVector.convertToGridPoint());
						}
					}
				}
			}
		}
		return resultList;
	}
	
	/**
	 * P�lya abszol�t sz�less�ge
	 */
	private int width = 40;
	
	/**
	 * P�lya abszol�t magass�ga
	 */
	private int height = 32;
	
	/**
	 * P�ly�n tal�lhat� minirobotok list�ja
	 */
	private Set<MiniRobot> minirobots;
	
	/**
	 * P�ly�n tal�lhat� akad�lyok list�ja
	 */
	private Set<Obstacle> obstacles;
	
	/**
	 * P�ly�n tal�lhat� robotok list�ja
	 */
	private Set<Robot> robots;
	
	
	/**
	 * Singleton mint�hoz haszn�latos
	 */
	private static Track instance = new Track(); 
	
	/**
	 * Param�ter n�lk�li konstruktor
	 */
	
	/**
	 * P�lya inicializ�lts�g�t jelz� v�ltoz� (L�tez� p�ly�ra elemek lerak�sa a j�t�k ind�t�sa el�tt)
	 */
	private boolean initialized;
	
	/**
	 * P�lya param�ter n�lk�li konstruktora
	 */
	private Track() {
		//Singleton minta
		initialized=false;
		robots = new LinkedHashSet<Robot>();
		minirobots = new LinkedHashSet<MiniRobot>();
		obstacles = new LinkedHashSet<Obstacle>();
		grid = new ArrayList< ArrayList<GridPoint> >();

		View.getInstance().addDrawable(new TrackView());
	}
	
	/**
	 * Singleton minta, ezzel a met�dussal f�r hozz� a Robot �s a MiniRobot is a p�ly�hoz
	 * @return a Track-et (p�ly�t) adja vissza
	 */
	public static Track getInstance() {
		if (instance == null) {
			instance = new Track();
		}
		return instance;
	}
	
	/**
	 * A p�lya magass�g�t lehet lek�rdezni vele
	 * @return a p�lya magass�ga
	 */
	public int getHeight(){
		return height;
	}
	
	/**
	 * A p�lya sz�less�g�t lehet lek�rdezni vele
	 * @return a p�lya sz�less�ge
	 */
	public int getWidth(){
		return width;
	}
	
	/**
	 * Obstacle felv�tele, a p�lya Obstacle list�j�ba
	 * @param obstacle �tadott akad�ly
	 */
	public void addObstacle(Obstacle obstacle){
		obstacles.add(obstacle);
	}
	
	/**
	 * Obstacle t�rl�se, a p�lya Obstacle list�j�b�l
	 * @param obstacle a t�r�lni k�v�nt Obstacle
	 */
	public void removeObstacle(Obstacle obstacle){
		obstacles.remove(obstacle);
	}
	
	/**
	 * �j r�cspont hozz�ad�sa a p�ly�hoz
	 * @param gridPoint P�ly�hoz adand� r�cspont
	 */
	public void addGridPoint(GridPoint gridPoint){
	}
	
	/**
	 * MarsVector-r�l  GridPointra (r�cspont) konverzi�t v�gz� met�dus
	 * @param marsVector Konvert�land� abszol�t koordin�ta
	 * @return Konvert�l�s eredm�nye R�cspontban
	 */
	public GridPoint marsVectorToGridPoint(MarsVector marsVector){
		GridPoint returnGridPoint = null;
		int y = marsVector.getY();
		int x = marsVector.getX();
		
		//elbaszott indexel�s
		if (y<0 || y>=grid.size() || x<0 || x>=grid.get(y).size())
			return null;
		
		returnGridPoint = grid.get(marsVector.getY()).get(marsVector.getX());
		
		return returnGridPoint;
	}
	
	/**
	 * Grid felt�lt�se r�cspontokkal
	 */
	private void fillGrid(){
		for (int i=0; i<height; i++){	//Sorok sz�ma (Magass�g)
			ArrayList<GridPoint> gpLine = new ArrayList<GridPoint>();		// Egy sor GridPoint
			for (int j=0; j<width; j++){		//Oszlopok sz�ma (Sz�less�g)
				GridPoint gp = new GridPoint();
				gp.setCoords(new MarsVector(j, i));
				//Nagyon ronda, de m�k�d� hardk�dolt megold�s a p�lya fel�p�t�s�re
				gp.setValid(false);
				if (gp.getCoords().getY() >= 10 && gp.getCoords().getY() <= 11 && gp.getCoords().getX() > 10 && gp.getCoords().getX() < 30) {
					gp.setValid(true);
				}
				if (gp.getCoords().getY() >= 10 && gp.getCoords().getY() < 15 && gp.getCoords().getX()+gp.getCoords().getY() >= 20 && gp.getCoords().getX()+gp.getCoords().getY() <= 23) {
					gp.setValid(true);
				}
				if (gp.getCoords().getY() >= 16 && gp.getCoords().getY() <= 20 && gp.getCoords().getX() > 10 && gp.getCoords().getX() < 30 && (gp.getCoords().getX()+gp.getCoords().getY())%3 != 0) {
					gp.setValid(true);
				}
				if (gp.getCoords().getY() >= 12 && gp.getCoords().getY() <= 16 && gp.getCoords().getY()-6 <= gp.getCoords().getX() && gp.getCoords().getY()-3 >= gp.getCoords().getX()) {
					gp.setValid(true);
				}
				if (gp.getCoords().getY() >= 8 && gp.getCoords().getY() <= 22 && gp.getCoords().getX() >= 30 && gp.getCoords().getX() <= 32 && (gp.getCoords().getX()*gp.getCoords().getY()+3)%7 != 0) {
					gp.setValid(true);
				}
				if (gp.getCoords().getX() >= 15 && gp.getCoords().getX() <= 25 && gp.getCoords().getY() >= 8 && gp.getCoords().getY() <= 24 && (gp.getCoords().getY()*gp.getCoords().getX()+5)%8 == 0) {
					gp.setValid(true);
				}
				
				gpLine.add(gp);
			}
			
			grid.add(gpLine);			// Soronk�nt hozz�adjuk az egyes GridPoint list�kat a grid-hez
		}
	}
	
	/**
	 * P�lya inicializ�l�sa
	 */
	public void initialize(){
		if(initialized==false){
			//createTrack(); //p�lya l�trehoz�sa
			fillGrid();		//grid felt�lt�se r�cspontokkal
		}

			robots=new LinkedHashSet<Robot>();
			GridPoint gp=new MarsVector(20,10).convertToGridPoint();
			new Robot(1,gp);
			gp=new MarsVector(20,11).convertToGridPoint();
			new Robot(2,gp);
			grid.get(10).get(10).addElement(new Oil(new MarsVector(20,18),20));
			grid.get(10).get(10).addElement(new Oil(new MarsVector(12,15),20));
			grid.get(10).get(10).addElement(new Oil(new MarsVector(31,15),20));
			grid.get(10).get(10).addElement(new Goo(new MarsVector(17,17),20));
			grid.get(10).get(10).addElement(new Goo(new MarsVector(21,17),20));
			for (int i=14; i<=16; i++) {
				grid.get(10).get(10).addElement(new Goo(new MarsVector(i,11),20));
			}
			initialized=true;
	}
		
	/**
	 * Akad�lyokat k�r�nk�nt �rtes�t� met�dus, hogy az ilyenkor esed�kes v�ltoztat�sok (�letpontcs�kken�sek) megval�suljanak
	 */
	public void notifyObservers(){
		
		// Ha sz�ks�ges, �j MiniRobot l�trehoz�sa (k�r�nk�nt max 1db)
		if(obstacles.size() > minirobots.size()*2){
			// MiniRobot poz�ci�j�nak sorsol�sa
			GridPoint landingPoint;
			boolean isCorrectZone = false;
			do {	// Biztons�g kedv��rt egy do-while ciklusban ellen�rizz�k, hogy a p�ly�n bel�lre kapjuk a koordin�t�kat		
				int x = (int)(Math.random()*98765%width);
				int y = (int)(Math.random()*56789%height);
				landingPoint = marsVectorToGridPoint(new MarsVector(x,y));
				
				if (landingPoint == null){	// HA �rv�nytelen r�cspont, akkor �jra
					continue;
				}
				
				if (landingPoint.getElements().isEmpty()){	// Ha valid �s �res a r�cspont akkor k�sz
					isCorrectZone = true;
				}
			} while(!isCorrectZone);
			
			new MiniRobot(landingPoint); // a MiniRobot lerak�si poz�ci�ja default kisz�mol�dik a konstrukor�ban �s mindenhova felirakozik
		}
		
		//MiniRobotoknak c�lpont friss�t�se --> nextTarget-ek be�ll�t�sa
		for(MiniRobot miniRobot: minirobots){
			miniRobot.setNextTarget();
		}
		
		// MiniRobotok l�ptet�se
		for(MiniRobot miniRobot : minirobots){
			miniRobot.move();
		}
	}
	
	/**
	 * Track karbantart� met�dusa. Minden k�r elej�n kifejti a sz�ks�ges hat�st a p�ly�n l�v� akad�lyokra (olaj sz�rad�sa)
	 */
	public void update(){
		//ez egy m�solat, ezen iter�lunk �s nem baj, ha k�zben az Obstacles-b�l t�rl�dik
		Set<Obstacle> obstaclesCopy = new LinkedHashSet<Obstacle>(obstacles);
		 
		// Akad�lyok friss�t�se
		for(Obstacle obstacle : obstaclesCopy){
			obstacle.notifyObstacle();
		}
		
	}
	
	/**
	 * GridPointok megsemmisit�se
	 */
	public void destroy(){
		//GridPoint-ok megsemmis�t�se
		for(ArrayList<GridPoint> gpList : grid){
			gpList.clear();
		}
		
		grid.clear();		//R�cs megsemmis�t�se
		obstacles.clear();	//Akad�lyok megsemmis�t�se
		minirobots.clear();	//MiniRobotok megsemmis�t�se
		robots.clear();		//Robotok megsemmis�t�se		
		instance = null;	//a singleton getInstance-e legk�zelebb �jabbat fog visszaadni
	}
	
	/**
	 * MiniRobot hozz�ad�sa a p�lya MiniRobot list�j�hoz
	 * @param minirobot a hozz�adni kiv�nt MiniRobot
	 */
	public void addMiniRobot(MiniRobot minirobot){
		minirobots.add(minirobot);
	}
	
	/**
	 * MiniRobot t�rl�se a p�lya MiniRobot list�j�r�l
	 * @param minirobot a t�r�lni k�v�nt MiniRobot
	 */
	public void removeMiniRobot(MiniRobot minirobot){
		minirobots.remove(minirobot);
	}
	
	/**
	 * Robot hozz�ad�sa a Robot list�hoz
	 * @param robot a hozz�adni k�v�nt Robot
	 */
	public void addRobot(Robot robot){
		robots.add(robot);
	}
	
	/**
	 * Robotok lek�rdez�se
	 * @return a p�lya list�j�ban szerepl� Robotok
	 */
	public Set<Robot> getRobots(){
		return robots;
	}
	
	/**
	 * Az akad�lyok lek�rdez�se
	 * @return az akad�lyok a p�lya list�j�b�l
	 */
	public Set<Obstacle> getObstacles(){
		return obstacles;
	}
	
}//v�ge