package hu.szoftlab4.rational;

import java.util.LinkedHashSet;
import java.util.Set;

/**
 * A p�lya alkot�elem�t: R�cspontot megval�s�t� oszt�ly
 * @author Daniel
 *
 */
public class GridPoint {
	
	/**
	 * A r�cspont abszol�t koordin�t�i MarsVector-k�nt t�rolva
	 */
	private MarsVector coords;
	
	/**
	 * R�csponton elhelyezked� Element-eket t�rol� lista (olaj, ragacs, robot)
	 */
	private Set<Element> elements;
	
	/**
	 * Logikai v�ltoz�, hogy a r�cspont a kijel�lt p�lya r�sze vagy sem.
	 */
	private boolean valid;
	
	/**
	 * Param�ter n�lk�li konstruktor
	 */
	public GridPoint(){
		elements = new LinkedHashSet<Element>();
	}
	
	/**
	 * R�cspontra valamilyen Element elhelyez�se (olaj, ragacs, robot)
	 * @param element R�cspontra elhelyezend� elem
	 */
	public void addElement(Element element){
			//Olaj lerak�shoz tartoz� szekvenci�hoz, pr�bamegold�s:
			elements.add(element);		
	}

	/**
	 * R�cspontr�l valamilyen Element elv�tele (Specifik�ci� szerint a verseny sor�n csak robotra h�vhat�.)
	 * @param element R�cspontr�l t�rlend� elem
	 */
	public void removeElement(Element element){
			elements.remove(element);
	}

	/**
	 * R�cspont abszol�t koordin�t�inak lek�rdez�se
	 * @return R�cspont abszol�t koordin�t�ja
	 */
	public MarsVector getCoords() {
			return coords;

	}

	/**
	 * R�cspont abszol�t koordin�t�inak be�ll�t�sa
	 * @param coords Abszol�t koordin�ta MarsVectorban
	 */
	public void setCoords(MarsVector coords) {
			
			this.coords = coords;
	}

	/**
	 * R�csponton elhelyezett Elementek lek�rdez�se list�ban
	 * @return R�csponton elhelyezett elemek list�ja
	 */
	public Set<Element> getElements() {
			return elements;
	}
	
	/**
	 * Lek�rdez�s, hogy a r�cspont a versenyp�lya r�sze-e?
	 * @return Versenyp�lya r�sze-e a r�cspont
	 */
	public boolean isValid() {
			return valid;
	}

	/**
	 * Annak be�ll�t�sa, hogy a r�cspont a p�lya r�sze-e vagy sem.
	 * @param valid P�lya eleme: true  / Nem eleme a p�ly�nak: false
	 */
	public void setValid(boolean valid) {
			this.valid = valid;
	}
	
}
