package hu.szoftlab4.rational;

/**
 * Alkalmazás Main osztálya
 * @author Daniel
 *
 */
public class Main {
	
	/**
	 * Alkalmazás belépési pontja
	 * @param args indítási argumentumok
	 */
	public static void main(String[] args) {
		new GameWindow();
		Game game=new Game();
		new Controller(game);
	}
}