package hu.szoftlab4.rational;

import java.awt.Color;

/**
 * Ragacs kirajzol�s��rt felel�s oszt�ly
 * @author Daniel
 *
 */
public class GooView extends ObstacleView{
	
	/**
	 * Param�teres konstruktor
	 * @param goo Referencia a hozz� tartoz� ragacsra
	 */
	public GooView(Goo goo){
		super(goo, Color.ORANGE);
	}

}
