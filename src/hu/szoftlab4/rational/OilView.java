package hu.szoftlab4.rational;

import java.awt.Color;

/**
 * Olaj kirajzol�s��rt felel�s oszt�ly
 * @author Daniel
 *
 */
public class OilView extends ObstacleView{
	
	/**
	 * Param�teres konstruktor
	 * @param oil Referencia a kirajzoland� olajra
	 */
	public OilView(Oil oil){
		super(oil, new Color(60, 80, 60));
	}

}
