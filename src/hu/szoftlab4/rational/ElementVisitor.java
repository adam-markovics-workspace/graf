package hu.szoftlab4.rational;

/**
 * Visitor Pattern megval�s�t�s�nak egyik interf�sze
 * Lehet�s�get biztos�t a p�ly�n halad� robotnak, hogy a k�l�nb�z� akad�lyok m�s-m�s hat�st v�ltsanak ki rajta
 * @author Daniel
 *
 */
public interface ElementVisitor {
	
	/**
	 * Robot ragacsra ugrik
	 * @param goo Robot �ltal �rintett ragacs
	 */
	public void visit(Goo goo);
	
	/**
	 * Robot olajra ugrik
	 * @param oil Robot �ltal �rintett olajfolt
	 */
	public void visit(Oil oil);
	
	/**
	 * Robot vagy MiniRobot egy  MiniRobotra ugrik
	 * @param miniRobot Az ugr�st v�gz� robot vagy minirobot �ltal �rintett m�sik minirobot
	 */
	public void visit(MiniRobot miniRobot);
		
	/**
	 * Robot m�sik robotra ugrik
	 * @param robot Az ugr�st v�gz� robot �ltal �rintett m�sik robot
	 */
	public void visit(Robot robot);
}
