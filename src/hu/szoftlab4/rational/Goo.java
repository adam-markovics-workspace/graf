package hu.szoftlab4.rational;

/**
 * Ragacsot megval�s�t� oszt�ly
 * @author Daniel
 *
 */
public class Goo extends Obstacle{
	
	/**
	 * Robot koptat�s�nak m�rt�ke amit a Robot a Ragacs l�togat�sakor �rv�nyes�t
	 */
	private static final int GOO_WEAR_RATE = 5;

	/**
	 * Param�ter n�lk�li konstruktor a Skeleton futtat�s�hoz
	 */
	public Goo(){
		this(new MarsVector(), 20);
	}
	
	/**
	 * Ragacs param�terezett konstruktora
	 * @param pos Inicializ�lt koordin�ta
	 * @param i Inicializ�lt �letpontok
	 */
	public Goo(MarsVector pos, int i) {
		super(pos, i);
		addView(new GooView(this));
	}

	/**
	 * Visitor patternt megval�s�t� met�dus
	 * Jelzi a r�ugr� robot sz�m�ra, hogy olyan r�cspontra �rkezett amelyen ragacs tal�lhat�
	 */
	@Override
	public void accept(ElementVisitor elementVisitor) {

			//Ragacsra l�p�s szeki
			elementVisitor.visit(this);
	}

	
	/**
	 * Ragacs k�r�nk�nti �rtes�t�s��rt felel�s met�dus
	 */
	@Override
	public void notifyObstacle() {
		// Jelenleg �res, mert az id� (k�r�k eltelte) nincs hat�ssal a ragacsra.
	}

	/**
	 * Ragacs �let�nek cs�kkent�se r�l�p�skor
	 */
	public void stepOn() {
		this.decreaseLifePoints(GOO_WEAR_RATE);		
	}

}
