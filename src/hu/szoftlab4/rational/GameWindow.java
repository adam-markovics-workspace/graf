package hu.szoftlab4.rational;

import java.awt.Dimension;

import javax.swing.JFrame;

/**
 * Alkalmazás futtatásának a "kerete"-t megvalósító osztály
 * @author Daniel
 *
 */
public class GameWindow extends JFrame{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Ablak szélessége
	 */
	static final int  SCREEN_WIDTH  =800;
	
	/**
	 * Ablak magassága
	 */
	static final int  SCREEN_HEIGHT =600;
	
	/**
	 * Konstruktor az alkalmazás ablakának megvalósításához
	 */
	public GameWindow(){
		
		setSize(SCREEN_WIDTH, SCREEN_HEIGHT);
		setContentPane(View.getInstance());
		getContentPane().setPreferredSize(new Dimension(SCREEN_WIDTH, SCREEN_HEIGHT));
		setLocationRelativeTo(null);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setResizable(false);
		this.pack();
		this.setTitle("Phoebe");
		this.setVisible(true);
	}
	

}
