package hu.szoftlab4.rational;

import hu.szoftlab4.rational.Game.GameState;
import hu.szoftlab4.rational.Robot.RobotState;
import hu.szoftlab4.rational.View.ViewState;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.List;
import java.util.Set;

/**
 * MVC pattern szerinti Controller oszt�ly megval�s�t�s bej�v� esem�nyek (eg�rkattint�sok) kezel�s�re
 * @author Daniel
 *
 */
public class Controller implements MouseListener{

	/**
	 * Referencia a Game oszt�lyra
	 */
	private Game game;
	
	/**
	 * Referencia a View oszt�lyra (minden kirajzolhat� objektum �se)
	 */
	private static View view;
	
	/**
	 * Param�teres konstruktora a Controller oszt�lynak
	 * @param game Be�ll�tand� Game oszt�ly
	 */
	public Controller(Game game){
		  this.game=game;
		  view=View.getInstance();
		  robotList = Track.getInstance().getRobots();
		  view.update(robotList,Game.getCurrentTurn());
		  view.addMouseListener(this);
		  ButtonListener bL=new ButtonListener();
		  view.gooButton.addActionListener(bL);
		  view.oilButton.addActionListener(bL);
		  game.runGame();
		 }
	
	/**
	 * Gombesem�nyek�rt felel�s bels� oszt�ly
	 * @author Daniel
	 *
	 */
	class ButtonListener implements ActionListener {

		/**
		 * Gombok esem�nykezel�j�nek met�dusa
		 */
	    public void actionPerformed(ActionEvent e) {
	     
	     Set<Robot> robotlist=Track.getInstance().getRobots();
	     
	      if (e.getSource()==view.gooButton) {
	       for(Robot robot: robotlist){
	        if(robot.getState()==RobotState.IDLING){
	         robot.useGoo();
	        }
	       }
	      }
	         
	      else if (e.getSource()==view.oilButton) {
	       for(Robot robot: robotlist){
	       if(robot.getState()==RobotState.IDLING){
	        robot.useOil();
	       }
	       }
	      }
	    }
	 }
		  
	
	@Override
	public void mouseClicked(MouseEvent arg0) {
		// TODO Auto-generated method stub		
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub		
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub	
	}

	/**
	 * Robotok list�ja amit minden k�rben friss�teni kell
	 */
	private Set<Robot> robotList;
	

	/**
	 * Eg�rlenyom�st kezel� met�dus
	 */
	@Override
	public void mousePressed(MouseEvent e) {
		
		if(game.getState()==GameState.RUNNING){			
			MarsVector mv=new MarsVector((e.getX()-80)/16,(e.getY()-120)/12);
			GridPoint gp=mv.convertToGridPoint();
			
			if (gp == null){
				return; //kikattintottunk a p�ly�b�l
			}
			
			for(Robot robot: robotList){
				if(robot.getState()==RobotState.IDLING){
					List<GridPoint> reachable=Track.getInstance().getReachableGridPoints(robot);
					if(reachable.contains(gp)){
						robot.setSpeed((gp.getCoords().subtract(robot.getCurrentGridPoint().getCoords())));
						robot.setState(RobotState.MOVING);
						/*
						game.isGameOver();
						if(game.getState()==GameState.OVER){
							view.setState(View.ViewState.OVER);
						}
						*/
						view.update(robotList,Game.getCurrentTurn());
						try {
							Thread.sleep(100);
						} catch (InterruptedException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						
						view.repaint();
						/*
						game.isGameOver();
						if(game.getState()==GameState.OVER){
							view.setState(ViewState.OVER);
							view.repaint();
						}*/
						break;
					}
/*
					game.isGameOver();
					if(game.getState()==GameState.OVER){
						view.setState(ViewState.OVER);
						view.repaint();
					}*/
				}
			}
		}		
	}

	/**
	 * Kif�lr�l h�vhat� j�t�k v�ge �llapot (minden k�r v�g�n force-olhat� a Game-b�l)
	 */
	public static void displayResults(){
		view.setState(ViewState.OVER);
		view.update(Game.getRobots(),Game.getCurrentTurn());
		view.repaint();
	}
	
	/**
	 * K�perny�friss�t�s kik�nyszer�t�se a MiniRobot.kill()-b�l hogy elt�ntesse a m�r meghalt MiniRobot-ot
	 */
	public static void refreshScreen(){
		view.repaint();
	}

	/**
	 * Eg�rkattint�s elenged�s�t kezel� met�dus
	 */
	@Override
	public void mouseReleased(MouseEvent e) {
		Set<Robot> robotlist=Track.getInstance().getRobots();
		view.update(robotlist,Game.getCurrentTurn());
		try {
			Thread.sleep(100);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		view.repaint();

	}
		
}