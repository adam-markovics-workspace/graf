package hu.szoftlab4.rational;

/**
 * P�lya r�cspontjain elhelyezhet� Akad�lyok (Olajfolt, ragacs) gy�jt�oszt�lya
 * @author Daniel
 *
 */
public abstract class Obstacle implements Element {

	/**
	 * Akad�ly �letpontjai
	 */
	private int lifePoints;
	
	/**
	 * Akad�ly poz�ci�ja koordin�tak�nt
	 */
	private MarsVector position;
	
	/**
	 * Referencia az akad�ly kirajzol�s�t v�gz� objektumra
	 */
	private ObstacleView view;
	
	/**
	 * Akad�ly konstruktora
	 * @param pos Inicializ�lt koordin�ta
	 * @param i Inicializ�lt �letpontok
	 */
	public Obstacle(MarsVector pos, int i){
		position = pos;
		lifePoints = i;
		position.convertToGridPoint().addElement(this);
		Track.getInstance().addObstacle(this);
	}
	
	/**
	 * �letpontokat lek�rdez� met�dus
	 * @return visszat�r az �letpontokkal
	 */
	public int getLifePoints(){
		return lifePoints;
	}
	
	/**
	 * Visitor patternt megval�s�t� met�dus
	 * Jelzi a r�ugr� robot sz�m�ra, hogy olyan r�cspontra �rkezett amelyen valamilyen akad�ly tal�lhat�
	 */
	@Override
	abstract public void accept(ElementVisitor elementVisitor);
	
	/**
	 * Akad�lyok �letpontjainak cs�kken�s�t megval�s�t� met�dus
	 * @param decrease Ennyivel cs�kken az akad�ly �letpontjai
	 */
	 public void decreaseLifePoints(int decrease){
		this.lifePoints-=decrease;
		if(lifePoints<=0){
			this.kill();
		}		
	 }
	 
	 /**
	  * Akad�ly megsemmis�t�s�rt felel�s met�dus
	  */
	 public void kill(){
		position.convertToGridPoint().removeElement(this);
		Track.getInstance().removeObstacle(this);
		View.getInstance().removeDrawable(view);
	 }
	 
	 /**
	  * Akad�ly poz�ci�j�t lek�rdez� met�dus
	  * @return Akad�ly poz�ci� MarsVector-ban
	  */
	 public MarsVector getPosition() {
		 return position;
	 }
	 
	 /**
	  * Akad�lyhoz az �t kirajzol� objektum beregisztr�l�sa
	  * @param ov akad�lyt kirajzol� objektum
	  */
	 public void addView(ObstacleView ov){
		 view = ov;
		 View.getInstance().addDrawable(view);
	 }

	/**
	 * Az akad�ly k�r�nk�nti �rtes�t�s�re szolg�l� met�dus
	 */
	abstract public void notifyObstacle();
	
}
