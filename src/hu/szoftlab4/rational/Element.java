package hu.szoftlab4.rational;

/**
 * P�lya r�cspontjain jelen l�v� objektumokat (ragacs,olaj,robot) �sszefoglal� interf�sz
 * K�l�nb�z� hat�st fejtenek ki a r�juk ugr� Robotra
 * @author Daniel
 *
 */
public interface Element {

	/**
	 * Visitor pattern megval�s�t�s�nak egyik eleme
	 * A robot jelzi a megl�togatott element fel�, hogy egy egyazon r�csponton tart�zkodnak
	 * @param elementVisitor Element-re �rkez� robot objektuma
	 */
	public void accept(ElementVisitor elementVisitor);
	
	/**
	 * A saj�t megsemmis�l�s��rt felel�s �s k�zben kiregisztr�lja mag�t a Track list�ir�l
	 */
	public void kill();

}
