package hu.szoftlab4.rational;

import java.util.LinkedHashSet;
import java.util.Set;

/**
 * MiniRobot (takar�t�robot) oszt�lya
 * @author Daniel
 *
 */
public class MiniRobot extends Machine{
	/**
	 * Takar�t�s ereje (h�ny �letpontot vesz�t egy akad�ly 1 k�r takar�t�s hat�s�ra) 
	 */
	private final int CLEANING_POWER = 5; //Cillit Bang! �s a kosz elt�nik.
	
	/**
	 * El�z� r�cspont t�rol�sa, mell�ksz�m�t�sok v�gz�s�hez
	 */
	private GridPoint previousGridPoint; //mell�ksz�m�t�sokhoz, f�leg �tk�z�sek eset�n sz�ks�ges
	
	/**
	 * Tiszt�tand� legk�zelebbi akad�ly koordin�t�i (m�r ha van ilyen)
	 */
	private MarsVector nextTarget;
	
	/**
	 * Referencia a minirobot kirajzol�s�t v�gz� objektumra
	 */
	private MiniRobotView mview;
	
	/**
	 * Jelz�se a r��rkez� objektum (robot vagy minirobot) fel�, hogy egy minirobotra landolt az ugr�s sor�n
	 */
	@Override
	public void accept(ElementVisitor elementVisitor) {
		elementVisitor.visit(this);
	}
	
	/**
	 * Param�ter n�lk�li konstruktor
	 */
	public MiniRobot(){
		MarsVector landingZone = new MarsVector(20,13);
		this.currentGridPoint = landingZone.convertToGridPoint();
		this.currentGridPoint.addElement(this);
		View.getInstance().addDrawable(new MiniRobotView(this));
		Track.getInstance().addMiniRobot(this);
	}
	
	/**
	 * Param�teres konstruktor (�tadott kiindul�si r�csponttal)
	 * @param gridPoint Kiindul�si r�cspont
	 */
	public MiniRobot(GridPoint gridPoint){
		this.currentGridPoint = gridPoint;
		this.currentGridPoint.addElement(this);
		//View.getInstance().addDrawable(new MiniRobotView(this));
		this.addView(new MiniRobotView(this));
		Track.getInstance().addMiniRobot(this);
	}
	
	/**
	 * MiniRobot mozg�s�t le�r� met�dus
	 */
	@Override
	public void move() {
		MarsVector targetVector = nextTarget.subtract(currentGridPoint.getCoords());
		//targetVector.subtractFromThis(currentGridPoint.getCoords());
		previousGridPoint = currentGridPoint;
		//GridPoint gp2 = currentGridPoint; //A k�vetkez� r�cspont, amit a jelenlegib�l m�dos�tva kapunk meg
		if (targetVector.getX() != 0) { 	//El�sz�r a v�zszintes s�kon mozog a MiniRobot
			if (targetVector.getX() < 0) {
				//Balra...
				MarsVector nextHop = currentGridPoint.getCoords().add(new MarsVector(-1,0));
				currentGridPoint = nextHop.convertToGridPoint();	
			}
			else {
				//...vagy jobbra
				MarsVector nextHop = currentGridPoint.getCoords().add(new MarsVector(1,0)); 
				currentGridPoint = nextHop.convertToGridPoint();
			}
		}
		else if (targetVector.getY() != 0) { //Ha m�r v�zszintesen egy vonalban van az akad�llyal, de f�gg�legesen m�g nem
			if (targetVector.getY() < 0) {
				//Lefel� mozog a MiniRobot...
				MarsVector nextHop = currentGridPoint.getCoords().add(new MarsVector(0,-1)); 
				currentGridPoint = nextHop.convertToGridPoint();
			}
			else {
				//...vagy felfel�
				MarsVector nextHop = currentGridPoint.getCoords().add(new MarsVector(0,1)); 
				currentGridPoint = nextHop.convertToGridPoint();
			}		
		}
	
		if (currentGridPoint!=previousGridPoint){		// X vagy Y tengelyen mozogtunk a k�rben --> M�g nem vagyunk rajta a c�lon
			Set<Element> elementsCopy = new LinkedHashSet<Element>(currentGridPoint.getElements());
            for (Element element : elementsCopy){
                element.accept(this);
            }
			
			if(previousGridPoint!=currentGridPoint){			// Igen, itt t�nyleg m�g egyszer megn�zz�k (b�r an�lk�l is m�k�dik, csak picit kev�sb� optimaliz�lt)
				previousGridPoint.removeElement(this);				// Ugr�s t�nyleges elv�gz�se
				currentGridPoint.addElement(this);			
			}
			previousGridPoint = currentGridPoint;		//TODO: Ez mi�rt kell ? Met�dus elej�n �s v�g�n is be�ll�tjuk ?
		
		} else {		// Nem mozogtunk arr�bb --> Megfelel� helyen vagyunk
			// Ha m�r az akad�lyt tartalmaz� r�csponton van
			// Takar�t�s: accept-h�v�s minden elemre (itt persze csak pontosan egy 
			// akad�lyra fejti ki a hat�s�t)
			// "Landol�s" ut�n az �j r�csponton az elemek bej�r�sa, kiv�ve �nmagunkra
            Set<Element> elementsCopy = new LinkedHashSet<Element>(currentGridPoint.getElements());
            for(Element e: elementsCopy){

				if(e!=this){
					e.accept(this);
				}
			}
		}
	}
	
	
	/**
	 * Tiszt�tand� akad�ly koordin�t�it be�ll�t� met�dus (nextTarget be�ll�t�sa) 
	 */
	public void setNextTarget(){
		
		double minDistance= 10000.0;
		boolean firstDistance = true;
		MarsVector targetMV = currentGridPoint.getCoords();
		for(Obstacle obstacle: Track.getInstance().getObstacles()){
			double currentObstacleDistance = obstacle.getPosition().subtract(currentGridPoint.getCoords()).length();
			if(firstDistance || currentObstacleDistance < minDistance){
				targetMV = obstacle.getPosition();
				minDistance = currentObstacleDistance;
				firstDistance = false;
			}
		}	
		this.nextTarget = targetMV;
	}
	
	/**
	 * Ragacsra �rkez�s elszenved�s�t kezel� met�dus
	 */
	@Override
	public void visit(Goo goo) {
		//Ha �ppen (Mini)Robot-nak �tk�z�s miatt visitel�nk, akkor nem teljes�l a felt�tel,
		//�s nem t�rt�nik semmi. K�l�nben takar�t�s.
		if (previousGridPoint == currentGridPoint) {
			goo.decreaseLifePoints(CLEANING_POWER);
		}
	}

	/**
	 * Olajra �rkez�s elszenved�s�t kezel� met�dus
	 */
	@Override
	public void visit(Oil oil) {
		//Ha �ppen (Mini)Robot-nak �tk�z�s miatt visitel�nk, akkor nem teljes�l a felt�tel,
		//�s nem t�rt�nik semmi. K�l�nben takar�t�s.
		if (previousGridPoint == currentGridPoint) {
			oil.decreaseLifePoints(CLEANING_POWER);
		}
	}

	/**
	 * MiniRobotra �rkez�s elszenved�s�t kezel� met�dus
	 */
	@Override
	public void visit(MiniRobot miniRobot) {
		if (miniRobot != this) {
			setCurrentGridPoint(previousGridPoint);
		}
	}

	/**
	 * Robotra �rkez�s elszenved�s�t kezel� met�dus
	 */
	@Override
	public void visit(Robot robot) { //Szinte teljesen ugyanaz, mint a MiniRobot-ra val� visit.
		setCurrentGridPoint(previousGridPoint);
	}

	 /**
	  * Minirobothoz az �t kirajzol� objektum beregisztr�l�sa
	  * @param mv minirobotot kirajzol� objektum
	  */
	 public void addView(MiniRobotView mv){
		 mview = mv;
		 View.getInstance().addDrawable(mview);
	 }
	 
	/**
	 * MiniRobot megsemmis�l�s��rt felel�s met�dus
	 */
	public void kill(){		
		currentGridPoint.removeElement(this);
		Track.getInstance().removeMiniRobot(this);
		View.getInstance().removeDrawable(mview);
		Controller.refreshScreen();
	}
}
